import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import './style.scss'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import { routes } from './router';
import store  from './store/store';
import axios from 'axios';
import Vuelidate from 'vuelidate';
export const eventBus = new Vue();
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuelidate);
// Vue.http.options.root = 'https://vuejs-stock-trader-2306d.firebaseio.com/';
axios.defaults.baseURL = 'https://vuejs-stock-trader-2306d.firebaseio.com/';
axios.interceptors.request.use(config => {
  /* eslint-disable */
  // console.log(config);
  return config;
});
Vue.filter('currency', (value) => {
  return '$' + value.toLocaleString()
})
const router = new VueRouter({
  mode: 'history',
  routes
})
Vue.use(BootstrapVue);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
