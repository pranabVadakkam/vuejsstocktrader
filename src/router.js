import Home from './components/home.vue';
import Portfolio from './components/portfolios/portfolio.vue';
import Stocks from './components/stocks/stocks.vue';
import Signup from './components/signup.vue';
import Login from './components/login.vue';
import store from './store/store';
export const routes = [
    {
        path: '/',
        component: Login,
        beforeEnter: (to, from, next) => {
            if (store.state.user.token) {
                next('/home');
            } else {
                next();
            }
        }
    },
    {
        path: '/home',
        component: Home,
        beforeEnter: (to, from, next) => {
            if (store.state.user.token) {
                next();
            } else {
                next('/');
            }
        }
    },
    {
        path: '/portfolio',
        component: Portfolio,
        beforeEnter: (to, from, next) => {
            if (store.state.user.token) {
                next();
            } else {
                next('/');
            }
        }
    },
    {
        path: '/stock',
        component: Stocks,
        beforeEnter: (to, from, next) => {
            if (store.state.user.token) {
                next();
            } else {
                next('/');
            }
        }
    },
    {
        path: '/signUp',
        component: Signup,
        beforeEnter: (to, from, next) => {
            if (store.state.user.token) {
                next('/home');
            } else {
                next();
            }
        }
    }
]