export const user = (state) => state.user;
export const isAuth = (state) => state.user.token !== null;