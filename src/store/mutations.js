import { eventBus }from '../main';

export const login =  (state, params) => {
    state.user.id = params.localId;
    state.user.token = params.idToken;
    eventBus.$emit('redirect', '/home');
};

export const logOut = (state) => {
    state.user.id = null;
    state.user.token = null;
    eventBus.$emit('redirect', '/');
};
