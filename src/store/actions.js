// import Vue from 'vue';
import axios from 'axios';

export const loadData = ({commit, state}) => {
    // Vue.http.get('data.json').then(res => res.json())
    // .then(response => {
    //     if (response) {
    //         const stocks = response.stocks;
    //         const funds = response.funds;
    //         const stockPortFolio = response.stockPortfolio ? response.stockPortfolio : [];
    //         const portFolio = {
    //             funds,stockPortFolio
    //         };
    //         commit('setStocks', stocks);
    //         commit('SET_PORTFOLIO', portFolio);
    //     }
    // })
    if(!state.user.token)
    {
        return 
    }  
    axios.get('data.json' + '?auth='+ state.user.token).then(res => res.data)
    .then(response => {
        if (response) {
            const stocks = response.stocks;
            const funds = response.funds;
            const stockPortFolio = response.stockPortfolio ? response.stockPortfolio : [];
            const portFolio = {
                funds,stockPortFolio
            };
            commit('setStocks', stocks);
            commit('SET_PORTFOLIO', portFolio);
        }
    });
}

export const signUp = ({commit, dispatch}, payload) => {
    axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyA3_cT0YFZW-Ja9x_td_1XiOb0-iNBp34Y', payload)
            .then(res => {

                const now = new Date();
                const expireTime = new Date(now.getTime() + res.data.expiresIn * 1000);
                localStorage.setItem('token', res.data.idToken);
                localStorage.setItem('userId', res.data.localId);
                localStorage.setItem('expiresIn', expireTime);
                commit('login', {localId: res.data.localId, idToken: res.data.idToken});
                dispatch('autoLogout', res.data.expiresIn * 1000);
            })
            .catch(error => error);
}

export const signIn = ({commit, dispatch}, payload) => {
    axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA3_cT0YFZW-Ja9x_td_1XiOb0-iNBp34Y', payload)
            .then(res => {
                const now = new Date();
                const expireTime = new Date(now.getTime() + res.data.expiresIn * 1000);
                localStorage.setItem('token', res.data.idToken);
                localStorage.setItem('userId', res.data.localId);
                localStorage.setItem('expiresIn', expireTime);
                commit('login', {localId: res.data.localId, idToken: res.data.idToken});
                dispatch('autoLogout', res.data.expiresIn * 1000);
            })
            .catch(error => error);
};
export const logOut = ({commit}) => {
    localStorage.removeItem('userId');
    localStorage.removeItem('token');
    localStorage.removeItem('expiresIn');
    commit('logOut');
};
export const autoLogout = ({dispatch}, payload) => {
    setTimeout(() => {
        dispatch('logOut');
    }, payload);
};

export const autoLogin = ({commit}) => {
    const token = localStorage.getItem('token');
    if (!token) {
        return;
    }   
    const expireTime = localStorage.getItem('expiresIn');
    const now = new Date();
    if (now >= expireTime) {
        return ;
    }
    const userId = localStorage.getItem('userId');
    commit('login', {localId: userId, idToken: token});
}

