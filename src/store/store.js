import Vue from 'vue';
import Vuex from 'vuex';
import stocks from './modules/stock';
import portfolio from './modules/portfolio';
import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        user: {
            id: null,
            token: null
        }
    },
    modules: {
        stocks,
        portfolio,
    },
    actions,
    mutations,
    getters
})