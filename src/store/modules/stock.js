import stocks from '../../data/stocks.js';

const state = {
    stocks: []
}

const mutations = {
    setStocks: (state, stocks) => {
        state.stocks = stocks;
    },
    randStock: (state) => {
        state.stocks.forEach(item => {
            item.price = Math.round(item.price * (1 + Math.random() - 0.5))
        })
    }
}

const actions = {
    buyStock: ({commit}) => {
        commit();   
    },
    initStock: ({commit}) => {
        commit('setStocks', stocks);   
    },
    randStock: ({commit}) => {
        commit('randStock');   
    }
}

const getters = {
    stocks: state => state.stocks
}

export default {
    state,
    mutations,
    actions,
    getters
}