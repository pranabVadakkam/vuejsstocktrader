const state ={
    stocks: [],
    funds: 100000
}

const mutations = {
    'BUY_STOCK'(state, {stockId, quantity, price}) {
        const record = state.stocks.find(elem => elem.id === stockId); 
        if (record) {
            record.quantity += quantity
        } else {
            state.stocks.push({id: stockId, quantity});
        }
        state.funds -= quantity * price;
    },
    'SELL_STOCK'(state,{stockId, quantity, price}) {
        const record = state.stocks.find(elem => elem.id === stockId); 
        if (record.quantity > quantity) {
            record.quantity -= quantity;
        } else {
            state.stocks.splice(state.stocks.indexOf(record), 1);
        }
        state.funds += quantity * price;
    },
    'SET_PORTFOLIO'(state, portFolio) {
        state.funds = portFolio.funds;
        state.stocks = portFolio.stockPortFolio;
    }

};

const actions = {
    sellStock({commit}, payload) {
        commit('SELL_STOCK', payload);
    },
    buyStocks({commit}, payload) {
        commit('BUY_STOCK', payload);
    }
}
const getters = {
    stockPortfolio: (state, getters) => {
        return state.stocks.map((item) => {
            const record = getters.stocks.find(elem => elem.id === item.id);
            return {
                id: item.id,
                quantity: item.quantity,
                name: record.name,
                price: record.price
            }
        });
    },
    funds: (state) => state.funds
}
export default {
    state,
    mutations,
    actions,
    getters
}